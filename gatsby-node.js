const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default

const {
  processNode,
  normaliseFieldName,
  mapMediaToNodes,
  mapProductsToCategories,
  mapProductsToTags,
  mapRelatedProducts,
  mapGroupedProducts,
  fixGraphqlTypes,
  asyncGetProductVariations,
  timeStampedLog,
} = require("./helpers")

const { getWCNodeId, mapNodeNormalizeMetadata } = require("./helpers/misc-data")
exports.sourceNodes = async (
  { actions, createNodeId, createContentDigest, store, cache },
  configOptions
) => {
  const { createNode, touchNode } = actions
  delete configOptions.plugins

  const {
    api,
    https,
    api_keys,
    fields,
    api_version = "wc/v3",
    per_page,
    wpAPIPrefix = null,
    query_string_auth = false,
    port = "",
    encoding = "",
    axios_config = null,
    verbose = true,
    ignore_fetch_error = false,
  } = configOptions

  // set up WooCommerce node api tool
  const WooCommerce = new WooCommerceRestApi({
    url: `http${https ? "s" : ""}://${api}`,
    consumerKey: api_keys.consumer_key,
    consumerSecret: api_keys.consumer_secret,
    version: api_version,
    wpAPIPrefix,
    queryStringAuth: query_string_auth,
    port,
    encoding,
    axiosConfig: axios_config,
  })

  // Fetch Node data for a given field name
  const fetchNodes = async fieldName => {
    let data_ = []
    let page = 1
    let pages

    do {
      let args = per_page ? { per_page, page } : { page }
      await WooCommerce.get(fieldName, args)
        .then(response => {
          if (response.status === 200) {
            data_ = [...data_, ...response.data]
            pages = parseInt(response.headers["x-wp-totalpages"])
            page++
          } else {
            const error_msg = `
              gatsby-source-woocommerce:
              ========== ERROR FOR FIELD ${fieldName} ===========
              The following error status was produced: ${response.data}
              ================== END WARNING ==================
              `
            if (ignore_fetch_error) {
              console.warn(`
                <IgnoredError>
                  ${error_msg}
                </IgnoredError>
              `)
            }
            else {
              throw new Error(`${error_msg}`)
            }
            return []
          }
        })
        .catch(error => {
          const error_msg = `
          gatsby-source-woocommerce:
          ========== ERROR FOR FIELD ${fieldName} ===========
          The following error status was produced: ${error}
          ================== END WARNING ==================
          `
          if (ignore_fetch_error) {
            console.warn(`
              <IgnoredError>
                ${error_msg}
              </IgnoredError>
            `)
          }
          else {
            throw new Error(`${error_msg}`)
          }
          return []
        })
    } while (page <= pages)

    return data_
  }

  // Loop over each field set in configOptions and process/create nodes
  async function fetchNodesAndCreate(array) {
    let nodes = []
    for (const field of array) {
      const fieldName = normaliseFieldName(field)
      let tempNodes = await fetchNodes(field)
      if (!Array.isArray(tempNodes) || !tempNodes.length) {
        throw new Error(`gatsby-source-woocommerce: Empty node array for: ${fieldName}`)
      } else if (tempNodes.length < 0 || tempNodes.length > 1000) {
        throw new Error(`gatsby-source-woocommerce: Invalid node array length of ${tempNodes.length} for: ${fieldName}`)
      }

      if (verbose) {
        timeStampedLog(
          `gatsby-source-woocommerce: Fetching ${tempNodes.length} nodes for field: ${field}`
        )
      }
      tempNodes = tempNodes.map(node => ({
        ...node,
        id: createNodeId(
          `woocommerce-${fieldName}-${getWCNodeId(node, fieldName)}`
        ),
        wordpress_id: getWCNodeId(node, fieldName),
        wordpress_parent_id: node.parent,
        __type: `wc${fieldName[0].toUpperCase() + fieldName.slice(1)}`,
      }))
      nodes = nodes.concat(tempNodes)
      if (verbose) {
        timeStampedLog(
          `gatsby-source-woocommerce: Completed fetching nodes for field: ${field}`
        )
      }
    }
    nodes = await asyncGetProductVariations(nodes, WooCommerce, verbose, ignore_fetch_error)
    nodes = await mapMediaToNodes({
      nodes,
      store,
      cache,
      createNode,
      createNodeId,
      touchNode,
      ignore_fetch_error,
    })

    let startTime = new Date().getTime()

    nodes = mapProductsToCategories(nodes)
    nodes = mapProductsToTags(nodes)
    nodes = mapRelatedProducts(nodes)
    nodes = mapGroupedProducts(nodes)
    nodes = fixGraphqlTypes(nodes, createNodeId)
    nodes = mapNodeNormalizeMetadata(nodes)

    nodes = nodes.map(node =>
      processNode(createContentDigest, node, verbose, ignore_fetch_error, startTime)
    )

    nodes.forEach(node => {
      createNode(node)
    })
    if (verbose) {
      timeStampedLog(
        `gatsby-source-woocommerce: ${
        nodes.length
        } nodes mapped, processed, and created in ${(new Date().getTime() -
          startTime) /
        1000}s`
      )
    }
  }

  await fetchNodesAndCreate(fields)
  return
}

exports.createSchemaCustomization = ({ actions, schema }, configOptions) => {
  const { createTypes } = actions
  const { fields } = configOptions

  let typeDefs = fields.map(field => {
    const fieldName = normaliseFieldName(field)
    const fieldType = `wc${fieldName[0].toUpperCase() + fieldName.slice(1)}`

    return schema.buildObjectType({
      name: fieldType,
      fields: {
        wordpress_parent: {
          type: fieldType,
          resolve(source, args, context, info) {
            return context.nodeModel
              .getAllNodes({ type: fieldType })
              .find(node => node.wordpress_id === source.wordpress_parent_id)
          },
        },
        wordpress_children: {
          type: `[${fieldType}]`,
          resolve(source, args, context, info) {
            return context.nodeModel
              .getAllNodes({ type: fieldType })
              .filter(node => node.wordpress_parent_id === source.wordpress_id)
          },
        },
      },
      interfaces: ["Node"],
    })
  })

  const productFieldType = "wcProducts"
  const acfFieldType = "wcProductsAcf"

  // Link bundled_items to their product nodes
  typeDefs.push(
    schema.buildObjectType({
      name: productFieldType,
      fields: {
        bundled_items: {
          type: `[${productFieldType}]`,
          resolve(source, args, context, info) {
            return context.nodeModel
              .getAllNodes({ type: productFieldType })
              .filter(node => {
                if (source.bundled_items.length > 0) {
                  return source.bundled_items.some(
                    it => it.product_id === node.wordpress_id
                  )
                } else {
                  return false
                }
              })
          },
        },
      },
      interfaces: ["Node"],
    })
  )

  // Link slected_bases to their product nodes
  typeDefs.push(
    schema.buildObjectType({
      name: acfFieldType,
      fields: {
        selected_bases: {
          type: `[${productFieldType}]`,
          resolve(source, args, context, info) {
            return context.nodeModel
              .getAllNodes({ type: productFieldType })
              .filter(
                node =>
                  source.selected_bases &&
                  source.selected_bases.includes(node.wordpress_id)
              )
          },
        },
      },
      interfaces: ["Node"],
    })
  )

  const promotionsFieldType = "localWpGraphQlBogofsBogofRule"
  // Link promotional_items to their product nodes
  typeDefs.push(
    schema.buildObjectType({
      name: promotionsFieldType,
      fields: {
        free_product_ids: {
          type: `[${productFieldType}]`,
          resolve(source, args, context, info) {
            return context.nodeModel
              .getAllNodes({ type: productFieldType })
              .filter(
                node =>
                  source.free_product_ids &&
                  source.free_product_ids.includes(node.wordpress_id)
              )
          },
        },
      },
      interfaces: ["Node"],
    })
  )

  createTypes(typeDefs)
}
