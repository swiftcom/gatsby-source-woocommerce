/* eslint-disable no-underscore-dangle */
const { createRemoteFileNode } = require(`gatsby-source-filesystem`)
const { createFileNode } = require(`gatsby-source-filesystem/create-file-node`)
const path = require(`path`)
const fs = require(`fs-extra`)
const Url = require(`url`)
const { createContentDigest } = require(`gatsby-core-utils`)
const promiseRetry = require('promise-retry');

const CONCURRENT_QUERIES = 250

/**
 * Add date and time stamp to message before logging to console.
 *
 * @param {string} message
 */
const timeStampedLog = message => {
  console.log(
    `${
    new Date().toISOString().match(/(\d\d\:\d\d\:\d\d\.\d\d\d\w)/)[0]
    } > ${message}`
  )
}

/**
 * Get a parsed URL from a string URL.
 *
 * @param {string} url
 */
function getParsedPath(url) {
  return path.parse(Url.parse(url).pathname)
}

// @8ctopotamus customization
const imageExtensions = [".jpg", ".jpeg", ".png", ".gif"]

let processNodeTimer = 0
const processNode = (
  createContentDigest,
  node,
  verbose,
  ignore_fetch_error,
  processNodeStartTime
) => {
  if (0 === processNodeTimer) {
    processNodeTimer = processNodeStartTime
  }

  const { __type } = node
  delete node.__type

  if (__type === "wcProducts" && node.categories) {
    node.categories.forEach(category => {
      // Add wordpress_id field when there is no
      // categories connection to keep the id access
      // consistent between just products & products with
      // categories
      category.wordpress_id = category.id
    })
  }

  const nodeContent = JSON.stringify(node)

  const nodeData = Object.assign({}, node, {
    id: node.id,
    wordpress_id: node.wordpress_id,
    parent: null,
    children: [],
    internal: {
      type: __type,
      contentDigest: createContentDigest(nodeContent),
    },
  })

  if (verbose) {
    //print progress every 1500 ms
    if (new Date().getTime() - processNodeTimer > 1500) {
      processNodeTimer = new Date().getTime()
      if (__type === "wcProducts") {
        timeStampedLog(
          `gatsby-source-woocommerce: Importing product { id: ${node.id}, name: ${node.name} }, with ${node.variations.length} variations`
        )
      } else {
        timeStampedLog(
          `gatsby-source-woocommerce: Importing { id: ${node.id}, name: ${node.name} }`
        )
      }
    }
  }

  return nodeData
}

/**
 * Get product variations data for variable products and add to nodes.
 * Asynchronous function.
 *
 * @param {array} nodes
 * @param {object} WooCommerce
 *
 * @return {array} Processed nodes
 */
const asyncGetProductVariations = async (nodes, WooCommerce, verbose, ignore_fetch_error) => {
  const asyncGetProductVariationsStartTime = new Date().getTime()

  let nodePromises = []
  for (
    let nodesIndex = 0;
    nodesIndex < nodes.length;
    nodesIndex += CONCURRENT_QUERIES
  ) {
    const endIndex =
      nodesIndex + CONCURRENT_QUERIES > nodes.length
        ? nodes.length
        : nodesIndex + CONCURRENT_QUERIES
    if (verbose) {
      timeStampedLog(
        `gatsby-source-woocommerce: Fetching ${nodesIndex} to ${endIndex} product variations for nodes`
      )
    }
    const nodePromises = nodes.slice(nodesIndex, endIndex).map(node => {
      if (node.__type === "wcProducts") {
        if (node.variations && node.variations.length) {
          node.product_variations = []
          const variations_path = `products/${node.wordpress_id}/variations?per_page=100`

          return promiseRetry(function (retry, number) {
            return WooCommerce.get(variations_path)
            .catch(response => {
              console.warn(`Warning: retry fetching variations for ${node.name}...`)
              retry()
            });
        }, {retries: 3, randomize: true}).then(response => {
              if (response.status === 200) {
                node.product_variations = [
                  ...node.product_variations,
                  ...response.data,
                ]
              } else {
                const error_msg = `
                Warning: error while fetching variations for ${node.name}.
                Error data: ${response.data}.
                `
                if (ignore_fetch_error) {
                  console.warn(`
                    <IgnoredError>
                      ${error_msg}
                    </IgnoredError>
                  `)
                }
                else {
                  throw new Error(`${error_msg}`)
                }
              }
            })
            .catch(error => {
              const error_msg = `
              gatsby-source-woocommerce: error while fetching variations for ${node.name}.
              Error: ${error}.
              `
              if (ignore_fetch_error) {
                console.warn(`
                <IgnoredError>
                  ${error_msg}
                </IgnoredError>
              `)
              }
              else {
                throw new Error(`${error_msg}`)
              }


            })
        } else {
          node.product_variations = []
        }
      }
    })
    await Promise.all(nodePromises)
    await new Promise(resolve => setTimeout(resolve, 500)); // a magical one-liner that waits for one second
  }

  timeStampedLog(
    `gatsby-source-woocommerce: Product variations retrieved for ${
    nodes.length
    } nodes in ${(new Date().getTime() - asyncGetProductVariationsStartTime) /
    1000}s`
  )
  return nodes
}

/**
 * Create links between products and categories (bi-directional)
 * @param {array} nodes
 *
 * @return {array} Processed nodes
 */
const mapProductsToCategories = nodes => {
  const categories = nodes.filter(
    node => node.__type === "wcProductsCategories"
  )

  return nodes.map(node => {
    if (categories.length && node.__type === "wcProducts") {
      node.categories.forEach(({ id }) => {
        const category = categories.find(c => id === c.wordpress_id)
        if (category) {
          if (!node.categories___NODE) {
            // Initialise the connection array if necessary
            node.categories___NODE = []
          }
          // Add the current category ID to the connection array
          node.categories___NODE.push(category.id)

          if (!category.products___NODE) {
            // Initialise the product connection array if necessary
            category.products___NODE = []
          }
          // Add the current product's ID to the connection array
          category.products___NODE.push(node.id)
        }
      })
      if (node.categories___NODE) {
        // Remove the old categories field if
        // nodes are now being referenced
        delete node.categories
      }
    }
    return node
  })
}

/**
 * Create links between products and tags (bi-directional)
 *
 * @param {array} nodes
 *
 * @return {array} Processed nodes
 */
const mapProductsToTags = nodes => {
  const tags = nodes.filter(node => node.__type === "wcProductsTags")

  return nodes.map(node => {
    if (tags.length && node.__type === "wcProducts") {
      node.tags.forEach(({ id }) => {
        const tag = tags.find(t => id === t.wordpress_id)
        if (tag) {
          if (!node.tags___NODE) {
            // Initialise the connection array if necessary
            node.tags___NODE = []
          }
          // Add the current tag ID to the connection array
          node.tags___NODE.push(tag.id)

          if (!tag.products___NODE) {
            // Initialise the connection array if necessary
            tag.products___NODE = []
          }

          //Add the current product's ID to the connection array
          tag.products___NODE.push(node.id)
        }
      })
      if (node.tags___NODE) {
        // Remove the old tags field if
        // nodes are now being referenced
        delete node.tags
      }
    }
    return node
  })
}

/**
 * Map nodes of related products to products
 *
 * @param {array} nodes
 *
 * @return {array} Processed nodes
 */
const mapRelatedProducts = nodes => {
  const products = nodes.filter(node => node.__type === "wcProducts")

  return nodes.map(node => {
    if (node.__type === "wcProducts") {
      const related_products = node.related_ids
        ? node.related_ids.map(id => {
          const product = products.find(
            product => product.wordpress_id === id
          )
          return product ? product.id : null
        })
        : null
      if (related_products) {
        node.related_products___NODE = related_products
      } else {
        node.related_products = []
      }
    }
    return node
  })
}

/**
 * Map nodes of each product in a grouped product to the parent product.
 *
 * @param {array} nodes
 *
 * @return {array} Processed nodes
 */
const mapGroupedProducts = nodes => {
  const products = nodes.filter(node => node.__type === "wcProducts")

  return nodes.map(node => {
    if (node.__type === "wcProducts") {
      const grouped_products = node.grouped_products
        ? node.grouped_products.map(id => {
          const product = products.find(
            product => product.wordpress_id === id
          )
          return product ? product.id : null
        })
        : null
      if (grouped_products) {
        node.grouped_products_nodes___NODE = grouped_products
      } else {
        node.grouped_products_nodes = []
      }
    }
    return node
  })
}

/**
 * Turn multi part endpoints into camelCase
 * i.e products/categories becomes productsCategories
 *
 * @param {string} name Non-normalised field name (e.g. products/categories)
 *
 * @return The camelCase field name
 */
const normaliseFieldName = name => {
  const parts = name.split("/")
  return parts.reduce((whole, partial) => {
    if (whole === "") {
      return whole.concat(partial)
    }
    return whole.concat(partial[0].toUpperCase() + partial.slice(1))
  }, "")
}

const setMedia = async ({
  n,
  image,
  store,
  cache,
  memoryCache,
  touchNode,
  createNode,
  createNodeId,
  ignore_fetch_error,
}) => {
  // Check RAM cache if this image has already been identified
  if (memoryCache[image.id]) {
    image.localFile___NODE = memoryCache[image.id]
    touchNode({ nodeId: memoryCache[image.id] })
    return
  }
  let fileNodeID

  // Build the key we use to mark cached images
  const mediaDataCacheKey = `wordpress-media-${image.id}`
  const url = image.src

  // Check if image exists in the traditional Gatsby cache
  const cacheMediaData = await cache.get(mediaDataCacheKey)
  if (cacheMediaData) {
    fileNodeID = cacheMediaData.fileNodeID
    if (fileNodeID) {
      image.localFile___NODE = fileNodeID
      touchNode({ nodeId: fileNodeID })
      memoryCache[image.id] = fileNodeID
      return
    }
  }

  // Otherwise, manually check if the image file is in cache
  const cacheDir = path.join(
    store.getState().program.directory,
    ".cache",
    "gatsby-source-filesystem"
  )
  await fs.ensureDir(cacheDir)
  const digest = createContentDigest(url)
  const parsedPath = getParsedPath(url)
  const filename = path.join(cacheDir, digest, parsedPath.name + parsedPath.ext)

  if (fs.existsSync(filename)) {
    // File exists. Create a file node with it
    const fileNode = await createFileNode(filename, createNodeId, {})
    fileNode.internal.description = `File "${url}"`
    fileNode.internal.contentDigest = digest
    fileNode.url = url
    fileNode.parent = n.id.toString()

    await createNode(fileNode, {
      name: `gatsby-source-filesystem`,
    })
    fileNodeID = fileNode.id
  } else {
    // Download the file from the server instead
    try {
      const fileNode = await createRemoteFileNode({
        url,
        store,
        cache,
        createNode,
        createNodeId,
        parentNodeId: n.id.toString(),
      })

      if (fileNode) {
        fileNodeID = fileNode.id
      }
    } catch (e) {
      const error_msg = `
      gatsby-source-woocommerce: FAILED to download image.
      Error: ${e}.
    `
      if (ignore_fetch_error) {
        console.warn(`
        <IgnoredError>
          ${error_msg}
        </IgnoredError>
      `)
      }
      else {
        throw new Error(`${error_msg}`)
      }
    }
  }

  // Once we have a "file" node that relates to this image, link it
  // Also add it to the Gatsby cache
  // Also add it to the RAM cache
  if (fileNodeID) {
    image.localFile___NODE = fileNodeID
    await cache.set(mediaDataCacheKey, {
      fileNodeID,
    })
    memoryCache[image.id] = fileNodeID
  }
}

// @8ctopotamus customization
const downloadACFMedia = async ({
  n,
  field,
  src,
  store,
  cache,
  touchNode,
  createNode,
  createNodeId,
}) => {
  let fileNodeID
  const mediaDataCacheKey = `woocommerce-acf-media-${src}`
  const cacheMediaData = await cache.get(mediaDataCacheKey)

  if (cacheMediaData) {
    fileNodeID = cacheMediaData.fileNodeID
    touchNode({ nodeId: fileNodeID })
  }

  if (!fileNodeID) {
    try {
      const fileNode = await createRemoteFileNode({
        url: src,
        store,
        cache,
        createNode,
        createNodeId,
        parentNodeId: n.id.toString(),
      })

      if (fileNode) {
        fileNodeID = fileNode.id

        await cache.set(mediaDataCacheKey, {
          fileNodeID,
        })
      }
    } catch (e) {
      // Ignore
    }
  }
  if (fileNodeID) {
    n.acf[field + "_localFile___NODE"] = fileNodeID
  }
}

const mapMediaToNodes = async ({
  nodes,
  store,
  cache,
  createNode,
  createNodeId,
  touchNode,
  ignore_fetch_error,
}) => {
  let memoryCache = []
  return Promise.all(
    nodes.map(async n => {
      const commonParams = {
        n,
        store,
        cache,
        touchNode,
        createNode,
        createNodeId,
      }

      if (n.product_variations && n.product_variations.length) {
        for await (let variation of n.product_variations) {
          const { image } = variation
          if (image) {
            await setMedia({
              image,
              memoryCache,
              ignore_fetch_error,
              ...commonParams,
            })
          }
        }
      }

      // @8ctopotamus customization
      if (n.acf) {
        Object.entries(n.acf).forEach(async entry => {
          const [field, val] = entry
          if (val && typeof val === "string") {
            const isImage = imageExtensions.some(ext => val.endsWith(ext))
            if (isImage) {
              await downloadACFMedia({
                field,
                memoryCache,
                src: val,
                ...commonParams,
              })
            }
          }
        })
      }

      if (n.images && n.images.length) {
        for (let image of n.images) {
          await setMedia({
            image,
            memoryCache,
            ignore_fetch_error,
            ...commonParams,
          })
        }
        return n
      } else if (n.image && n.image.id) {
        const { image } = n
        await setMedia({
          image,
          memoryCache,
          ignore_fetch_error,
          ...commonParams,
        })

        return n
      } else {
        return n
      }
    })
  )
}

/**
 * Manually and with brute force, fix field types
 *
 * @param {array} nodes
 *
 * @return {array} Processed nodes
 */
const fixGraphqlTypes = (nodes, createNodeId) => {
  const vatPercentage = 1.15
  return nodes.map((node, index) => {
    node.price = parseInt(Math.round(parseFloat(node.price) * vatPercentage))
    node.regular_price = parseInt(Math.round(parseFloat(node.regular_price) * vatPercentage))
    node.sale_price = parseInt(Math.round(parseFloat(node.sale_price) * vatPercentage))
    if (node.acf) {
      // if (typeof node.acf.comfort_level != "object") {
      // 	node.acf.comfort_level = {}
      // }
      if (typeof node.acf.comfort_trial_available != "string") {
        node.acf.comfort_trial_available = "NA"
      }
      if (!Array.isArray(node.acf.bedroom_type)) {
        node.acf.bedroom_type = []
      }
      if (!node.acf.id) {
        node.acf.id = createNodeId(`node-acf-${index}`)
      }
      if (!Array.isArray(node.acf.selected_bases)) {
        node.acf.selected_bases = []
      }
      if (!Array.isArray(node.acf.bed_bundle_size_images)) {
        node.acf.bed_bundle_size_images = []
      }
    }
    if (!Array.isArray(node.attributes)) {
      node.attributes = []
    }
    if (!Array.isArray(node.product_variations)) {
      node.product_variations = []
    }
    node.product_variations = node.product_variations.map(variation => {
      variation.price = parseInt(Math.round(parseFloat(variation.price) * vatPercentage))
      variation.regular_price =
        parseInt(Math.round(parseFloat(variation.regular_price) * vatPercentage))
      variation.sale_price = parseInt(Math.round(parseFloat(variation.sale_price) * vatPercentage))
      if (!variation.attributes) {
        variation.attributes = []
      }
      if (variation.manage_stock == "parent") {
        variation.manage_stock = false
      }
      return variation
    })
    return node
  })
}

module.exports = {
  processNode,
  normaliseFieldName,
  mapMediaToNodes,
  mapProductsToCategories,
  mapProductsToTags,
  mapRelatedProducts,
  mapGroupedProducts,
  fixGraphqlTypes,
  asyncGetProductVariations,
  timeStampedLog,
}
